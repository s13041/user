package main;

import java.util.List;

import model.Person;
import user.managerDatabase.PersonManagerDB;

public class Test {

	public static void main(String[] args) {
		System.out.println("Test");

		PersonManagerDB personDB = new PersonManagerDB();

		List<Person> listaPerson = personDB.getAll();

		for (Person p : listaPerson) {
			System.out.println(p.getId() + " " + p.getImie() + " "+p.getNazwisko());
		}
		personDB.deleteById(3);

		Person toUpdate = new Person();

		toUpdate.setImie("Misio");
		toUpdate.setNazwisko("Ogorek");

		personDB.update(toUpdate);
		
		System.out.println("Koniec testu");

	}

}
